#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QMediaRecorder>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QMediaRecorder *recorder = new QMediaRecorder(&app);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.loadFromModule("qmediarecorder-crash-demo", "Main");

    return app.exec();
}
