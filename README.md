gdb stacktrace:

```sh
Thread 1 "appqmediarecord" received signal SIGSEGV, Segmentation fault.
0x00007ffff5906a34 in XDisplayString () from /usr/lib/libX11.so.6
(gdb) backtrace 
#0  0x00007ffff5906a34 in XDisplayString () at /usr/lib/libX11.so.6
#1  0x00007fffd654aa1d in __vaDriverInit_1_13 () at /usr/lib/dri/nvidia_drv_video.so
#2  0x00007fffe8ced27c in va_openDriver (driver_name=<optimized out>, dpy=0x555555cb3e60)
    at ../libva/va/va.c:478
#3  va_new_opendriver (dpy=0x555555cb3e60) at ../libva/va/va.c:714
#4  vaInitialize (minor_version=0x7fffffffd030, major_version=0x7fffffffd034, dpy=0x555555cb3e60)
    at ../libva/va/va.c:743
#5  vaInitialize (dpy=0x555555cb3e60, major_version=0x7fffffffd034, minor_version=0x7fffffffd030)
    at ../libva/va/va.c:727
#6  0x00007fffe9443aad in  () at /usr/lib/qt6/plugins/multimedia/../../../libavutil.so.58
#7  0x00007fffe9430aa2 in av_hwdevice_ctx_create ()
    at /usr/lib/qt6/plugins/multimedia/../../../libavutil.so.58
#8  0x00007ffff00b4f66 in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#9  0x00007ffff00b583e in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#10 0x00007ffff00b6d92 in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#11 0x00007ffff00b6f77 in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#12 0x00007ffff00b7c91 in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#13 0x00007ffff00bc730 in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#14 0x00007ffff00bdc6b in  () at /usr/lib/qt6/plugins/multimedia/libffmpegmediaplugin.so
#15 0x00007ffff7ef1b75 in  () at /usr/lib/libQt6Multimedia.so.6
#16 0x00007ffff5aafbbf in __pthread_once_slow
    (once_control=0x555555691fa8, init_routine=0x7ffff5ce0230 <std::__once_proxy()>) at pthread_once.c:116
#17 0x00007ffff7f107f7 in QMediaRecorder::QMediaRecorder(QObject*) () at /usr/lib/libQt6Multimedia.so.6
#18 0x0000555555557830 in main(int, char**) (argc=1, argv=0x7fffffffd868)
    at /path/to/qmediarecorder-crash-demo/main.cpp:12
(gdb)
```
